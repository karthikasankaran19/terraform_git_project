terraform{
    backend "s3" {
    bucket         = "myappk-tfstate-bucket"
    key            = "global/s3/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "myapp-tfstate-locking"
    encrypt        = true
  }

}
