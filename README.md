# terraform_codepipeline

This project utilizes GitLab CI/CD pipeline and Terraform modules to deploy infrastructure in AWS. The Terraform state will be stored in AWS S3. The infrastructure that will be built is a VPC with 2 public subnets and an Autoscaling group of EC2 instances.

Ref:https://aws.plainenglish.io/gitlab-ci-cd-pipeline-with-terraform-5f591a2c5c21